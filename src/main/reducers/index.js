import { combineReducers } from 'redux';
import alphaCalculator from '../../modules/alphaCalculator';

export default combineReducers({
    [alphaCalculator.constants.NAME]: alphaCalculator.reducer
});