import alphaCalculator from '../../modules/alphaCalculator';

export default function* rootSaga() {
    yield [
        alphaCalculator.sagas()
    ];
}