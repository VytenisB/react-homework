import React from 'react';
import alphaCalculator from '../../modules/alphaCalculator';

const { AlphaCalculator} = alphaCalculator.containers;

const App = () => (
    <div className="App">
        <AlphaCalculator/>
    </div>
);

export default App;